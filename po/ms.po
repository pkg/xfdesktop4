# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# abuyop <abuyop@gmail.com>, 2022
# Xfce Bot <transifex@xfce.org>, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: xfdesktop\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-22 12:24+0100\n"
"PO-Revision-Date: 2022-12-22 11:24+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>, 2022\n"
"Language-Team: Malay (https://www.transifex.com/xfce/teams/16840/ms/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ms\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../settings/main.c:430 ../src/xfdesktop-special-file-icon.c:290
#: ../src/xfdesktop-special-file-icon.c:456
msgid "Home"
msgstr "Rumah"

#: ../settings/main.c:432 ../src/xfdesktop-special-file-icon.c:292
#: ../src/xfdesktop-special-file-icon.c:454
msgid "File System"
msgstr "Sistem Fail"

#: ../settings/main.c:434 ../src/xfdesktop-special-file-icon.c:294
msgid "Trash"
msgstr "Tong Sampah"

#: ../settings/main.c:436
msgid "Removable Devices"
msgstr "Peranti Boleh Ubah"

#: ../settings/main.c:438
msgid "Network Shares"
msgstr "Perkongsian Rangkaian"

#: ../settings/main.c:440
msgid "Disks and Drives"
msgstr "Cakera dan Peranti"

#: ../settings/main.c:442
msgid "Other Devices"
msgstr "Peranti Lain"

#. Display the file name, file type, and file size in the tooltip.
#: ../settings/main.c:546
#, c-format
msgid ""
"<b>%s</b>\n"
"Type: %s\n"
"Size: %s"
msgstr ""
"<b>%s</b>\n"
"Jenis: %s\n"
"Saiz: %s"

#: ../settings/main.c:744
#, c-format
msgid "Wallpaper for Monitor %d (%s)"
msgstr "Hiasan dinding untuk Monitor %d (%s)"

#: ../settings/main.c:747
#, c-format
msgid "Wallpaper for Monitor %d"
msgstr "Hiasan dinding untuk Monitor %d"

#: ../settings/main.c:753
msgid "Move this dialog to the display you want to edit the settings for."
msgstr "Alih dialog ini ke paparan yang anda mahu suntingkan tetapannya."

#: ../settings/main.c:760
#, c-format
msgid "Wallpaper for %s on Monitor %d (%s)"
msgstr "Hiasan dinding untuk %s pada Monitor %d (%s)"

#: ../settings/main.c:764
#, c-format
msgid "Wallpaper for %s on Monitor %d"
msgstr "Hiasan dinding untuk %s pada Monitor %d"

#: ../settings/main.c:771
msgid ""
"Move this dialog to the display and workspace you want to edit the settings "
"for."
msgstr ""
"Alih dialog ini ke paparan dan ruang kerja yang anda mahu suntingkan "
"tetapannya."

#. Single monitor and single workspace
#: ../settings/main.c:779
#, c-format
msgid "Wallpaper for my desktop"
msgstr "Hiasan dinding untuk desktop saya"

#. Single monitor and per workspace wallpaper
#: ../settings/main.c:785
#, c-format
msgid "Wallpaper for %s"
msgstr "Hiasan dinding untuk %s"

#: ../settings/main.c:790
msgid "Move this dialog to the workspace you want to edit the settings for."
msgstr "Pindah dialog ini ke ruangkerja yang anda mahu sunting tetapannya."

#: ../settings/main.c:1243
msgid "Image selection is unavailable while the image style is set to None."
msgstr "Pilihan imej tidak didapati ketika gaya imej ditetapkan ke Tiada."

#: ../settings/main.c:1603
msgid "Spanning screens"
msgstr "Rentang skrin"

#. TRANSLATORS: Please split the message in half with '\n' so the dialog will
#. not be too wide.
#: ../settings/main.c:1784
msgid ""
"Would you like to arrange all existing\n"
"icons according to the selected orientation?"
msgstr ""
"Anda mahu menyusun semua ikon\n"
"sedia ada berdasarkan orientasi pilihan?"

#: ../settings/main.c:1790
msgid "Arrange icons"
msgstr "Susun ikon"

#. printf is to be translator-friendly
#: ../settings/main.c:1798 ../src/xfdesktop-file-icon-manager.c:817
#: ../src/xfdesktop-file-icon-manager.c:1410
#, c-format
msgid "Unable to launch \"%s\":"
msgstr "Tidak dapat melancarkan \"%s\":"

#: ../settings/main.c:1799 ../src/xfdesktop-file-icon-manager.c:819
#: ../src/xfdesktop-file-icon-manager.c:1159
#: ../src/xfdesktop-file-icon-manager.c:1411 ../src/xfdesktop-file-utils.c:744
#: ../src/xfdesktop-file-utils.c:1313 ../src/xfdesktop-file-utils.c:1338
#: ../src/xfdesktop-file-utils.c:1445 ../src/xfdesktop-file-utils.c:1509
msgid "Launch Error"
msgstr "Ralat Dilancarkan"

#: ../settings/main.c:1801 ../settings/xfdesktop-settings-ui.glade.h:20
#: ../src/xfdesktop-file-icon-manager.c:585
#: ../src/xfdesktop-file-icon-manager.c:604
#: ../src/xfdesktop-file-icon-manager.c:689
#: ../src/xfdesktop-file-icon-manager.c:821
#: ../src/xfdesktop-file-icon-manager.c:1163
#: ../src/xfdesktop-file-icon-manager.c:1413
#: ../src/xfdesktop-file-icon-manager.c:3077 ../src/xfdesktop-file-utils.c:747
#: ../src/xfdesktop-file-utils.c:767 ../src/xfdesktop-file-utils.c:822
#: ../src/xfdesktop-file-utils.c:886 ../src/xfdesktop-file-utils.c:947
#: ../src/xfdesktop-file-utils.c:1008 ../src/xfdesktop-file-utils.c:1056
#: ../src/xfdesktop-file-utils.c:1111 ../src/xfdesktop-file-utils.c:1169
#: ../src/xfdesktop-file-utils.c:1256 ../src/xfdesktop-file-utils.c:1317
#: ../src/xfdesktop-file-utils.c:1340 ../src/xfdesktop-file-utils.c:1449
#: ../src/xfdesktop-file-utils.c:1513 ../src/xfdesktop-file-utils.c:1584
#: ../src/xfdesktop-file-utils.c:1654 ../src/xfdesktop-volume-icon.c:508
#: ../src/xfdesktop-volume-icon.c:554 ../src/xfdesktop-volume-icon.c:638
msgid "_Close"
msgstr "_Tutup"

#: ../settings/main.c:1961
msgid "Image files"
msgstr "Fail imej"

#. Change the title of the file chooser dialog
#: ../settings/main.c:1969
msgid "Select a Directory"
msgstr "Pilih satu Direktori"

#: ../settings/main.c:2178
msgid "Settings manager socket"
msgstr "Soket pengurus tetapan"

#: ../settings/main.c:2178
msgid "SOCKET ID"
msgstr "SOCKET ID"

#: ../settings/main.c:2179
msgid "Version information"
msgstr "Maklumat versi"

#: ../settings/main.c:2180 ../src/xfdesktop-application.c:848
msgid "Enable debug messages"
msgstr "Benarkan mesej nyahpepijat"

#: ../settings/main.c:2204
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Taip '%s --help' untuk penggunaan."

#: ../settings/main.c:2216
msgid "The Xfce development team. All rights reserved."
msgstr "Pasukan pembangunan Xfce. Semua hak cipta terpelihara."

#: ../settings/main.c:2217
#, c-format
msgid "Please report bugs to <%s>."
msgstr "Sila laporkan pepijat ke <%s>."

#: ../settings/main.c:2224
msgid "Desktop Settings"
msgstr "Tetapan Desktop"

#: ../settings/main.c:2226
msgid "Unable to contact settings server"
msgstr "Tidak dapat menyambung ke tetapan pelayan"

#: ../settings/main.c:2228
msgid "Quit"
msgstr "Keluar"

#: ../settings/xfce-backdrop-settings.desktop.in.in.h:1
#: ../settings/xfdesktop-settings-ui.glade.h:18 ../src/xfce-desktop.c:1158
msgid "Desktop"
msgstr "Desktop"

#: ../settings/xfce-backdrop-settings.desktop.in.in.h:2
msgid "Set desktop background and menu and icon behavior"
msgstr "Tetapkan latarbelakang desktop dan menu dan kelakuan ikon"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:1
msgid "Solid color"
msgstr "Warna padu"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:2
msgid "Horizontal gradient"
msgstr "Gradien mendatar"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:3
msgid "Vertical gradient"
msgstr "Gradien menegak"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:4
msgid "Transparent"
msgstr "Lutsinar"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:5
msgid "Choose the folder to select wallpapers from."
msgstr "Pilih folder untuk pilih kertas dinding di dalamnya."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:6
msgid "Apply"
msgstr "Laksana"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:7
msgid "St_yle:"
msgstr "Gaya:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:8
msgid "Specify how the image will be resized to fit the screen."
msgstr ""
"Nyatakan bagaimana imej akan disaiz semula untuk disuai muatkan pada skrin."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:9
#: ../settings/xfdesktop-settings-ui.glade.h:4
msgid "None"
msgstr "Tiada"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:10
msgid "Centered"
msgstr "Tengah"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:11
msgid "Tiled"
msgstr "Susun"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:12
msgid "Stretched"
msgstr "Meregang"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:13
msgid "Scaled"
msgstr "Diskala"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:14
msgid "Zoomed"
msgstr "Dizum"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:15
msgid "Spanning Screens"
msgstr "Rentang skrin"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:16
msgid "Specify the style of the color drawn behind the backdrop image."
msgstr "Nyatakan gaya warna yang dilukis disebalik imej tirai latar."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:17
msgid "Specifies the solid color, or the \"left\" or \"top\" color of the gradient."
msgstr "Nyatakan warna padu, atau warna \"kiri\" atau \"atas\" gradien."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:18
msgid "Select First Color"
msgstr "Pilih Warna Pertama"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:19
msgid "Specifies the \"right\" or \"bottom\" color of the gradient."
msgstr "Nyatakan warna \"kanan\" atau \"bawah\" gradien."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:20
msgid "Select Second Color"
msgstr "Pilih Warna Kedua"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:21
msgid "Apply to all _workspaces"
msgstr "Serap ke semua ruangkerja"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:22
msgid "_Folder:"
msgstr "_Folder:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:23
msgid "C_olor:"
msgstr "_Warna:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:24
msgid "Change the _background "
msgstr "Tukar latarbelakang "

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:25
msgid ""
"Automatically select a different background from the current directory."
msgstr ""
"Pilih latarbelakang berlainan daripada direktori sekarang secara automatik."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:26
msgid "Specify how often the background will change."
msgstr "Nyatakan berapa kerapkah latar belakang akan bertukar."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:27
msgid "in seconds:"
msgstr "dalam saat:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:28
msgid "in minutes:"
msgstr "dalam minit:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:29
msgid "in hours:"
msgstr "dalam jam:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:30
msgid "at start up"
msgstr "pada permulaan"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:31
msgid "every hour"
msgstr "setiap jam"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:32
msgid "every day"
msgstr "setiap hari"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:33
msgid "chronologically"
msgstr "kronologi"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:34
msgid "Amount of time before a different background is selected."
msgstr "Bilangan masa sebelum latarbelakang berlainan dipilih."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:35
msgid "_Random Order"
msgstr "Turutan _Rawak"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:36
msgid ""
"Randomly selects another image from the same directory when the wallpaper is"
" to cycle."
msgstr ""
"Pilih imej secara rawak dari direktori sama ketika kitaran hiasan dinding."

#: ../settings/xfdesktop-settings-ui.glade.h:1
msgid "Left"
msgstr "Kiri"

#: ../settings/xfdesktop-settings-ui.glade.h:2
msgid "Middle"
msgstr "Tengah"

#: ../settings/xfdesktop-settings-ui.glade.h:3
msgid "Right"
msgstr "Kanan"

#: ../settings/xfdesktop-settings-ui.glade.h:5
msgid "Shift"
msgstr "Shift"

#: ../settings/xfdesktop-settings-ui.glade.h:6
msgid "Alt"
msgstr "Alt"

#: ../settings/xfdesktop-settings-ui.glade.h:7
msgid "Control"
msgstr "Control"

#: ../settings/xfdesktop-settings-ui.glade.h:8
msgid "Minimized application icons"
msgstr "Minimakan ikon aplikasi"

#: ../settings/xfdesktop-settings-ui.glade.h:9
msgid "File/launcher icons"
msgstr "Ikon fail/pelancar"

#: ../settings/xfdesktop-settings-ui.glade.h:10
msgid "Top Left Vertical"
msgstr "Kiri Atas Menegak"

#: ../settings/xfdesktop-settings-ui.glade.h:11
msgid "Top Left Horizontal"
msgstr "Kiri Tegak Mengufuk"

#: ../settings/xfdesktop-settings-ui.glade.h:12
msgid "Top Right Vertical"
msgstr "Kanan Atas Menegak"

#: ../settings/xfdesktop-settings-ui.glade.h:13
msgid "Top Right Horizontal"
msgstr "Kanan Atas Mengufuk"

#: ../settings/xfdesktop-settings-ui.glade.h:14
msgid "Bottom Left Vertical"
msgstr "Kiri Bawah Menegak"

#: ../settings/xfdesktop-settings-ui.glade.h:15
msgid "Bottom Left Horizontal"
msgstr "Kiri Bawah Mengufuk"

#: ../settings/xfdesktop-settings-ui.glade.h:16
msgid "Bottom Right Vertical"
msgstr "Kanan Bawah Menegak"

#: ../settings/xfdesktop-settings-ui.glade.h:17
msgid "Bottom Right Horizontal"
msgstr "Kanan Bawah Mengufuk"

#: ../settings/xfdesktop-settings-ui.glade.h:19
msgid "_Help"
msgstr "_Bantuan"

#: ../settings/xfdesktop-settings-ui.glade.h:21
msgid "_Background"
msgstr "_Latar belakang"

#: ../settings/xfdesktop-settings-ui.glade.h:22
msgid "Enable \"Delete\" option in file context menu"
msgstr "Benarkan pilihan \"Padam\" dalam menu konteks fail"

#: ../settings/xfdesktop-settings-ui.glade.h:23
msgid "Include applications menu on _desktop right click"
msgstr "Sertakan menu aplikasi bila klik kanan _desktop"

#: ../settings/xfdesktop-settings-ui.glade.h:24
msgid "_Button:"
msgstr "Butang:"

#: ../settings/xfdesktop-settings-ui.glade.h:25
msgid "Mo_difier:"
msgstr "Pengubahsuai:"

#: ../settings/xfdesktop-settings-ui.glade.h:26
msgid "Show _application icons in menu"
msgstr "Papar ikon aplikasi pada menu"

#: ../settings/xfdesktop-settings-ui.glade.h:27
msgid "_Edit desktop menu"
msgstr "Sunting menu d_esktop"

#: ../settings/xfdesktop-settings-ui.glade.h:28
msgid "<b>Desktop Menu</b>"
msgstr "<b>Menu Desktop</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:29
msgid "Show _window list menu on desktop middle click"
msgstr "Papar tetingkap menu senarai pada klik tengah desktop"

#: ../settings/xfdesktop-settings-ui.glade.h:30
msgid "B_utton:"
msgstr "Butang:"

#: ../settings/xfdesktop-settings-ui.glade.h:31
msgid "Modi_fier:"
msgstr "Pengubahsuai:"

#: ../settings/xfdesktop-settings-ui.glade.h:32
msgid "Sh_ow application icons in menu"
msgstr "Papar ikon aplikasi dalam menu"

#: ../settings/xfdesktop-settings-ui.glade.h:33
msgid "Show workspace _names in list"
msgstr "Papar _nama ruang kerja dalam senarai"

#: ../settings/xfdesktop-settings-ui.glade.h:34
msgid "Use _submenus for the windows in each workspace"
msgstr "Guna submenu untuk tetingkap dalam setip ruangkerja"

#: ../settings/xfdesktop-settings-ui.glade.h:35
msgid "Show s_ticky windows only in active workspace"
msgstr "Papar tetingkap lekat hanya pada ruangkerja aktif"

#: ../settings/xfdesktop-settings-ui.glade.h:36
msgid "Show a_dd and remove workspace options in list"
msgstr "Tunjuk pilihan ta_mbah dan buang ruang kerja dalam senarai"

#: ../settings/xfdesktop-settings-ui.glade.h:37
msgid "<b>Window List Menu</b>"
msgstr "<b>Menu Senarai Tetingkap</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:38
msgid "_Menus"
msgstr "_Menu"

#: ../settings/xfdesktop-settings-ui.glade.h:39
msgid "Icon _type:"
msgstr "Jenis ikon:"

#: ../settings/xfdesktop-settings-ui.glade.h:40
msgid "Icon _size:"
msgstr "Saiz ikon:"

#: ../settings/xfdesktop-settings-ui.glade.h:41
msgid "48"
msgstr "48"

#: ../settings/xfdesktop-settings-ui.glade.h:42
msgid "Icons _orientation:"
msgstr "_Orientasi ikon:"

#: ../settings/xfdesktop-settings-ui.glade.h:43
msgid "Show icons on primary display"
msgstr "Tunjuk ikon atas paparan utama"

#: ../settings/xfdesktop-settings-ui.glade.h:44
msgid "Use custom _font size:"
msgstr "Guna saiz fon tersuai:"

#: ../settings/xfdesktop-settings-ui.glade.h:45
msgid "12"
msgstr "12"

#: ../settings/xfdesktop-settings-ui.glade.h:46
msgid "Show icon tooltips. Size:"
msgstr "Papar ikon tip alatan. Saiz:"

#: ../settings/xfdesktop-settings-ui.glade.h:47
msgid "Size of the tooltip preview image."
msgstr "Saiz tip alatan pratonton imej."

#: ../settings/xfdesktop-settings-ui.glade.h:48
msgid "64"
msgstr "64"

#: ../settings/xfdesktop-settings-ui.glade.h:49
msgid "Show t_humbnails"
msgstr "Papar imej kecil"

#: ../settings/xfdesktop-settings-ui.glade.h:50
msgid ""
"Select this option to display preview-able files on the desktop as "
"automatically generated thumbnail icons."
msgstr ""
"Pilih pilihan ini untuk memaparkan fail boleh pratonton pada desktop sebagai"
" ikon lakaran kecil yang dijana secara automatik."

#: ../settings/xfdesktop-settings-ui.glade.h:51
msgid "Show hidden files on the desktop"
msgstr "Tunjuk fail tersembunyi pada desktop"

#: ../settings/xfdesktop-settings-ui.glade.h:52
msgid "Single _click to activate items"
msgstr "Klik sekali untuk aktifkan item"

#: ../settings/xfdesktop-settings-ui.glade.h:53
msgid "<b>Appearance</b>"
msgstr "<b>Penampilan</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:54
msgid "<b>Default Icons</b>"
msgstr "<b>Ikon Lalai</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:55
msgid "_Icons"
msgstr "_Ikon"

#: ../src/menu.c:94
msgid "_Applications"
msgstr "_Aplikasi"

#: ../src/windowlist.c:75
#, c-format
msgid "Remove Workspace %d"
msgstr "Buang Ruang kerja %d"

#: ../src/windowlist.c:76
#, c-format
msgid ""
"Do you really want to remove workspace %d?\n"
"Note: You are currently on workspace %d."
msgstr ""
"Adakah anda mahu membuang ruangkerja %d?\n"
"Nota: Anda sekarang pada ruangkerja %d."

#: ../src/windowlist.c:80
#, c-format
msgid "Remove Workspace '%s'"
msgstr "Buang Ruang kerja '%s'"

#: ../src/windowlist.c:81
#, c-format
msgid ""
"Do you really want to remove workspace '%s'?\n"
"Note: You are currently on workspace '%s'."
msgstr ""
"Adakah anda mahu membuang ruangkerja '%s'?\n"
"Nota: Anda sekarang pada ruangkerja '%s'."

#. Popup a dialog box confirming that the user wants to remove a
#. * workspace
#: ../src/windowlist.c:88
msgid "Remove"
msgstr "Buang"

#: ../src/windowlist.c:283
msgid "Window List"
msgstr "Senarai Tetingkap"

#: ../src/windowlist.c:311
#, c-format
msgid "<b>Workspace %d</b>"
msgstr "<b>Ruangkerja %d</b>"

#: ../src/windowlist.c:407 ../src/windowlist.c:409
msgid "_Add Workspace"
msgstr "T_ambah Ruang kerja"

#: ../src/windowlist.c:417
#, c-format
msgid "_Remove Workspace %d"
msgstr "Buang _Ruang kerja %d"

#: ../src/windowlist.c:420
#, c-format
msgid "_Remove Workspace '%s'"
msgstr "Buang _Ruang kerja '%s'"

#: ../src/xfdesktop-application.c:840
msgid "Display version information"
msgstr "Papar maklumat versi"

#: ../src/xfdesktop-application.c:841
msgid "Reload all settings"
msgstr "Muat semula semua tetapan"

#: ../src/xfdesktop-application.c:842
msgid "Advance to the next wallpaper on the current workspace"
msgstr "Maju ke kertas dinding berikutnya pada ruang kerja semasa"

#: ../src/xfdesktop-application.c:843
msgid "Pop up the menu (at the current mouse position)"
msgstr "Timbulkan menu (pada kedudukan tetikus semasa)"

#: ../src/xfdesktop-application.c:844
msgid "Pop up the window list (at the current mouse position)"
msgstr "Timbulkan senarai tetingkap (pada kedudukan tetikus semasa)"

#: ../src/xfdesktop-application.c:846
msgid "Automatically arrange all the icons on the desktop"
msgstr "Susun semua ikon diatas desktop secara automatik"

#: ../src/xfdesktop-application.c:849
msgid "Disable debug messages"
msgstr "Lumpuhkan mesej nyahpepijat"

#: ../src/xfdesktop-application.c:850
msgid "Do not wait for a window manager on startup"
msgstr "Jangan tunggu pengurus tetingkap pada permulaan"

#: ../src/xfdesktop-application.c:851
msgid "Cause xfdesktop to quit"
msgstr "Sebab xfdesktop keluar"

#: ../src/xfdesktop-application.c:866
#, c-format
msgid "Failed to parse arguments: %s\n"
msgstr "Gagal menghurai argumen: %s\n"

#: ../src/xfdesktop-application.c:877
#, c-format
msgid "This is %s version %s, running on Xfce %s.\n"
msgstr "Ini adalah %s versi %s, berjalan dalam Xfce %s.\n"

#: ../src/xfdesktop-application.c:879
#, c-format
msgid "Built with GTK+ %d.%d.%d, linked with GTK+ %d.%d.%d."
msgstr "Binaan dengan GTK+ %d.%d.%d, dipaut dengan GTK+ %d.%d.%d."

#: ../src/xfdesktop-application.c:883
#, c-format
msgid "Build options:\n"
msgstr "Pilihan binaan:\n"

#: ../src/xfdesktop-application.c:884
#, c-format
msgid "    Desktop Menu:        %s\n"
msgstr "Menu Desktop: %s\n"

#: ../src/xfdesktop-application.c:886 ../src/xfdesktop-application.c:893
#: ../src/xfdesktop-application.c:900
msgid "enabled"
msgstr "dibenarkan"

#: ../src/xfdesktop-application.c:888 ../src/xfdesktop-application.c:895
#: ../src/xfdesktop-application.c:902
msgid "disabled"
msgstr "dilumpuhkan"

#: ../src/xfdesktop-application.c:891
#, c-format
msgid "    Desktop Icons:       %s\n"
msgstr "Ikon Desktop: %s\n"

#: ../src/xfdesktop-application.c:898
#, c-format
msgid "    Desktop File Icons:  %s\n"
msgstr "Ikon Fail Desktop: %s\n"

#: ../src/xfdesktop-file-icon-manager.c:577
#: ../src/xfdesktop-file-icon-manager.c:595
#, c-format
msgid "Could not create the desktop folder \"%s\""
msgstr "Tak dapat mencipta folder desktop \"%s\""

#: ../src/xfdesktop-file-icon-manager.c:582
#: ../src/xfdesktop-file-icon-manager.c:600
msgid "Desktop Folder Error"
msgstr "Ralat Folder Desktop"

#: ../src/xfdesktop-file-icon-manager.c:602
msgid ""
"A normal file with the same name already exists. Please delete or rename it."
msgstr ""
"Fail biasa dengan nama sama telah ada. Sila padam atau tukarkan namanya."

#: ../src/xfdesktop-file-icon-manager.c:686 ../src/xfdesktop-file-utils.c:818
#: ../src/xfdesktop-file-utils.c:882
msgid "Rename Error"
msgstr "Ralat Menamakan Semula"

#: ../src/xfdesktop-file-icon-manager.c:687 ../src/xfdesktop-file-utils.c:883
msgid "The files could not be renamed"
msgstr "Fail tidak boleh dinamakan semula"

#: ../src/xfdesktop-file-icon-manager.c:688
msgid "None of the icons selected support being renamed."
msgstr "Tiada ikon yang dipilih menyokong untuk dinamakan semula."

#: ../src/xfdesktop-file-icon-manager.c:1013
msgid ""
"This will reorder all desktop items and place them on different screen positions.\n"
"Are you sure?"
msgstr ""
"Tindakan ini akan menertib semula semua item atas-meja dan letak ia dalam kedudukan skrin berlainan.\n"
"Adakah anda pasti?"

#: ../src/xfdesktop-file-icon-manager.c:1017
msgid "_OK"
msgstr "_OK"

#: ../src/xfdesktop-file-icon-manager.c:1085
#, c-format
msgid "_Open With \"%s\""
msgstr "_Buka Dengan \"%s\""

#: ../src/xfdesktop-file-icon-manager.c:1088
#, c-format
msgid "Open With \"%s\""
msgstr "Buka Dengan \"%s\""

#: ../src/xfdesktop-file-icon-manager.c:1161
msgid ""
"Unable to launch \"exo-desktop-item-edit\", which is required to create and "
"edit launchers and links on the desktop."
msgstr ""
"Tidak dapat melancarkan \"exo-desktop-item-edit\", yang diperlukan untuk "
"mencipta dan menyunting pelancar dan capaian pada desktop."

#: ../src/xfdesktop-file-icon-manager.c:1526
msgid "_Open all"
msgstr "_Buka semua"

#: ../src/xfdesktop-file-icon-manager.c:1546
msgid "Create _Launcher..."
msgstr "Cipta Pe_lancar..."

#: ../src/xfdesktop-file-icon-manager.c:1560
msgid "Create _URL Link..."
msgstr "Cipta Capaian _URL..."

#: ../src/xfdesktop-file-icon-manager.c:1574
msgid "Create _Folder..."
msgstr "Cipta _Folder..."

#: ../src/xfdesktop-file-icon-manager.c:1586
msgid "Create _Document"
msgstr "Cipta _Dokumen"

#: ../src/xfdesktop-file-icon-manager.c:1611
msgid "No templates installed"
msgstr "Tiada templat dipasang"

#: ../src/xfdesktop-file-icon-manager.c:1627
msgid "_Empty File"
msgstr "Fail _Kosong"

#: ../src/xfdesktop-file-icon-manager.c:1637
#: ../src/xfdesktop-special-file-icon.c:523 ../src/xfdesktop-volume-icon.c:868
msgid "_Open"
msgstr "_Buka"

#: ../src/xfdesktop-file-icon-manager.c:1654
msgid "_Execute"
msgstr "_Lakukan"

#: ../src/xfdesktop-file-icon-manager.c:1672
msgid "_Edit Launcher"
msgstr "Sunting P_elancar"

#: ../src/xfdesktop-file-icon-manager.c:1731
msgid "Open With"
msgstr "Buka Dengan"

#: ../src/xfdesktop-file-icon-manager.c:1758
#: ../src/xfdesktop-file-icon-manager.c:1783
msgid "Open With Other _Application..."
msgstr "Buka Dengan _Aplikasi Lain..."

#: ../src/xfdesktop-file-icon-manager.c:1769
msgid "Set _Default Application..."
msgstr "Tetapkan Aplikasi _Lalai..."

#: ../src/xfdesktop-file-icon-manager.c:1801
msgid "_Paste"
msgstr "_Tampal"

#: ../src/xfdesktop-file-icon-manager.c:1819
msgid "Cu_t"
msgstr "Po_tong"

#: ../src/xfdesktop-file-icon-manager.c:1831
msgid "_Copy"
msgstr "_Salin"

#: ../src/xfdesktop-file-icon-manager.c:1843
msgid "Paste Into Folder"
msgstr "Tampal Ke Dalam Folder"

#: ../src/xfdesktop-file-icon-manager.c:1862
msgid "Mo_ve to Trash"
msgstr "A_lih ke Tong Sampah"

#: ../src/xfdesktop-file-icon-manager.c:1875
msgid "_Delete"
msgstr "Pa_dam"

#: ../src/xfdesktop-file-icon-manager.c:1893
msgid "_Rename..."
msgstr "_Nama Semula..."

#: ../src/xfdesktop-file-icon-manager.c:1958
msgid "_Open in New Window"
msgstr "_Buka pada Tetingkap Baharu"

#: ../src/xfdesktop-file-icon-manager.c:1967
msgid "Arrange Desktop _Icons"
msgstr "Menguruskan Ikon Desktop"

#: ../src/xfdesktop-file-icon-manager.c:1977
msgid "_Next Background"
msgstr "Latar Belakang _Berikutnya"

#: ../src/xfdesktop-file-icon-manager.c:1987
msgid "Desktop _Settings..."
msgstr "Tetapan De_sktop..."

#: ../src/xfdesktop-file-icon-manager.c:1995
#: ../src/xfdesktop-volume-icon.c:935
msgid "P_roperties..."
msgstr "C_iri-ciri..."

#: ../src/xfdesktop-file-icon-manager.c:3074
msgid "Load Error"
msgstr "Ralat Memuatkan"

#: ../src/xfdesktop-file-icon-manager.c:3076
msgid "Failed to load the desktop folder"
msgstr "Gagal untuk memuatkan folder desktop"

#: ../src/xfdesktop-file-icon-manager.c:3622
msgid "Copy _Here"
msgstr "Salin Sini"

#: ../src/xfdesktop-file-icon-manager.c:3622
msgid "_Move Here"
msgstr "_Pindah Sini"

#: ../src/xfdesktop-file-icon-manager.c:3622
msgid "_Link Here"
msgstr "_Paut Sini"

#: ../src/xfdesktop-file-icon-manager.c:3657
msgid "_Cancel"
msgstr "_Batal"

#: ../src/xfdesktop-file-icon-manager.c:3786
msgid "Untitled document"
msgstr "Dokumen tidak bertajuk"

#. TRANSLATORS: file was modified less than one day ago
#: ../src/xfdesktop-file-utils.c:170
#, c-format
msgid "Today at %X"
msgstr "Hari ini pada %X"

#. TRANSLATORS: file was modified less than two days ago
#: ../src/xfdesktop-file-utils.c:174
#, c-format
msgid "Yesterday at %X"
msgstr "Semalam pada %X"

#. Days from last week
#: ../src/xfdesktop-file-utils.c:179
#, c-format
msgid "%A at %X"
msgstr "%A pada %X"

#. Any other date
#: ../src/xfdesktop-file-utils.c:182
#, c-format
msgid "%x at %X"
msgstr "%x pada %X"

#. the file_time is invalid
#: ../src/xfdesktop-file-utils.c:192 ../src/xfdesktop-file-utils.c:1579
msgid "Unknown"
msgstr "Tidak diketahui"

#: ../src/xfdesktop-file-utils.c:320
#, c-format
msgid "%.*s (copy %u)%s"
msgstr "%.*s (salinan %u)%s"

#: ../src/xfdesktop-file-utils.c:745
msgid "The folder could not be opened"
msgstr "Folder tidak dapat dibuka"

#: ../src/xfdesktop-file-utils.c:764
msgid "Error"
msgstr "Ralat"

#: ../src/xfdesktop-file-utils.c:765
msgid "The requested operation could not be completed"
msgstr "Operasi diminta tidak dapat diselesaikan"

#: ../src/xfdesktop-file-utils.c:819
msgid "The file could not be renamed"
msgstr "Fail tidak boleh dinamakan semula"

#: ../src/xfdesktop-file-utils.c:820 ../src/xfdesktop-file-utils.c:884
#: ../src/xfdesktop-file-utils.c:945 ../src/xfdesktop-file-utils.c:1109
#: ../src/xfdesktop-file-utils.c:1167 ../src/xfdesktop-file-utils.c:1254
#: ../src/xfdesktop-file-utils.c:1315 ../src/xfdesktop-file-utils.c:1447
#: ../src/xfdesktop-file-utils.c:1511 ../src/xfdesktop-file-utils.c:1652
msgid ""
"This feature requires a file manager service to be present (such as the one "
"supplied by Thunar)."
msgstr ""
"Ciri ini memerlukan kehadiran perkhidmatan pengurus fail (seperti yang "
"dibekalkan oleh Thunar)."

#: ../src/xfdesktop-file-utils.c:943
msgid "Delete Error"
msgstr "Ralat Padam"

#: ../src/xfdesktop-file-utils.c:944
msgid "The selected files could not be deleted"
msgstr "Fail terpilih tidak dapat dipadam"

#: ../src/xfdesktop-file-utils.c:1004 ../src/xfdesktop-file-utils.c:1052
msgid "Trash Error"
msgstr "Ralat Sampah"

#: ../src/xfdesktop-file-utils.c:1005
msgid "The selected files could not be moved to the trash"
msgstr "Fail terpilih tidak dapat dialih ke dalam tong sampah"

#: ../src/xfdesktop-file-utils.c:1006 ../src/xfdesktop-file-utils.c:1054
msgid ""
"This feature requires a trash service to be present (such as the one "
"supplied by Thunar)."
msgstr ""
"Ciri ini memerlukan kehadiran perkhidmatan tong sampah.(seperti yang "
"dibekalkan oleh Thunar)."

#: ../src/xfdesktop-file-utils.c:1053
msgid "Could not empty the trash"
msgstr "Tidak dapat mengosongkan tong sampah"

#: ../src/xfdesktop-file-utils.c:1107
msgid "Create File Error"
msgstr "Ralat Mencipta Fail"

#: ../src/xfdesktop-file-utils.c:1108
msgid "Could not create a new file"
msgstr "Tidak dapat mencipta fail baharu"

#: ../src/xfdesktop-file-utils.c:1165
msgid "Create Document Error"
msgstr "Ralat Mencipta Dokumen"

#: ../src/xfdesktop-file-utils.c:1166
msgid "Could not create a new document from the template"
msgstr "Tidak dapat mencipta fail baharu daripada templat"

#: ../src/xfdesktop-file-utils.c:1252
msgid "File Properties Error"
msgstr "Ralat Ciri-ciri Fail"

#: ../src/xfdesktop-file-utils.c:1253
msgid "The file properties dialog could not be opened"
msgstr "Dialog ciri-ciri fail tidak dapat dibuka"

#: ../src/xfdesktop-file-utils.c:1314
msgid "The file could not be opened"
msgstr "Fail tidak dapat dibuka"

#: ../src/xfdesktop-file-utils.c:1335 ../src/xfdesktop-file-utils.c:1442
#, c-format
msgid "Failed to run \"%s\""
msgstr "Gagal menjalankan \"%s\""

#: ../src/xfdesktop-file-utils.c:1510
msgid "The application chooser could not be opened"
msgstr "Pemilih aplikasi tidak dapat dibuka"

#: ../src/xfdesktop-file-utils.c:1581 ../src/xfdesktop-file-utils.c:1650
msgid "Transfer Error"
msgstr "Ralat Pemindahan"

#: ../src/xfdesktop-file-utils.c:1582 ../src/xfdesktop-file-utils.c:1651
msgid "The file transfer could not be performed"
msgstr "Pemindahan fail tidak dapat dilaksanakan"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:127
msgid "Unmounting device"
msgstr "Menyahlekap peranti"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:130
#, c-format
msgid ""
"The device \"%s\" is being unmounted by the system. Please do not remove the"
" media or disconnect the drive"
msgstr ""
"Peranti \"%s\" sedang dinyahlekap oleh sistem. Sila jangan keluarkan media "
"atau tanggalkan pemacu"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:137 ../src/xfdesktop-notify.c:322
msgid "Writing data to device"
msgstr "Menulis data ke peranti"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:140 ../src/xfdesktop-notify.c:325
#, c-format
msgid ""
"There is data that needs to be written to the device \"%s\" before it can be"
" removed. Please do not remove the media or disconnect the drive"
msgstr ""
"Terdapat data yang perlu ditulis ke peranti \"%s\" sebelum ia boleh "
"dikeluarkan. Sila jangan keluarkan media atau tanggalkan pemacu"

#: ../src/xfdesktop-notify.c:221
msgid "Unmount Finished"
msgstr "Nyahlekap Selesai"

#: ../src/xfdesktop-notify.c:223 ../src/xfdesktop-notify.c:408
#, c-format
msgid "The device \"%s\" has been safely removed from the system. "
msgstr "Peranti \"%s\" telah selamat dikeluarkan dari sistem. "

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:313
msgid "Ejecting device"
msgstr "Melenting peranti"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:316
#, c-format
msgid "The device \"%s\" is being ejected. This may take some time"
msgstr "Peranti \"%s\" sedang dilenting. Ia mengambil sedikit masa"

#: ../src/xfdesktop-notify.c:406
msgid "Eject Finished"
msgstr "Selesai Dikeluarkan"

#: ../src/xfdesktop-regular-file-icon.c:795
#, c-format
msgid ""
"Name: %s\n"
"Type: %s\n"
"Size: %s\n"
"Last modified: %s"
msgstr ""
"Nama: %s\n"
"Jenis: %s\n"
"Saiz: %s\n"
"Ubahsuai terakhir: %s"

#: ../src/xfdesktop-special-file-icon.c:439
msgid "Trash is empty"
msgstr "Tong sampah adalah kosong"

#: ../src/xfdesktop-special-file-icon.c:442
msgid "Trash contains one item"
msgstr "Sampah mempunyai satu item"

#: ../src/xfdesktop-special-file-icon.c:443
#, c-format
msgid "Trash contains %d items"
msgstr "Sampah mempunyai %d item"

#: ../src/xfdesktop-special-file-icon.c:472
#, c-format
msgid ""
"%s\n"
"Size: %s\n"
"Last modified: %s"
msgstr ""
"%s\n"
"Saiz: %s\n"
"Ubahsuai terakhir: %s"

#: ../src/xfdesktop-special-file-icon.c:539
msgid "_Empty Trash"
msgstr "_Kosongkan Tong Sampah"

#: ../src/xfdesktop-volume-icon.c:458
msgid "(not mounted)"
msgstr ""

#: ../src/xfdesktop-volume-icon.c:459 ../src/xfdesktop-volume-icon.c:460
msgid "(unknown)"
msgstr "(tidak diketahui)"

#: ../src/xfdesktop-volume-icon.c:464
#, c-format
msgid ""
"Name: %s\n"
"Type: %s\n"
"Mounted at: %s\n"
"Size: %s\n"
"Free Space: %s"
msgstr ""

#: ../src/xfdesktop-volume-icon.c:466
msgid "Removable Volume"
msgstr ""

#: ../src/xfdesktop-volume-icon.c:501 ../src/xfdesktop-volume-icon.c:547
#, c-format
msgid "Failed to eject \"%s\""
msgstr "Gagal melenting \"%s\""

#: ../src/xfdesktop-volume-icon.c:506 ../src/xfdesktop-volume-icon.c:552
msgid "Eject Failed"
msgstr "Gagal Dikeluarkan"

#: ../src/xfdesktop-volume-icon.c:633
#, c-format
msgid "Failed to mount \"%s\""
msgstr "Gagal melekap \"%s\""

#: ../src/xfdesktop-volume-icon.c:636
msgid "Mount Failed"
msgstr "Gagal Dilekap"

#: ../src/xfdesktop-volume-icon.c:894
msgid "_Safely Remove Volume"
msgstr "Tanggal Volum secara _Selamat"

#: ../src/xfdesktop-volume-icon.c:897
msgid "_Disconnect Volume"
msgstr "_Tanggalkan Volum"

#: ../src/xfdesktop-volume-icon.c:900
msgid "_Stop the Multi-Disk Drive"
msgstr "_Henti Pemacu Cakera-Berbilang"

#: ../src/xfdesktop-volume-icon.c:903
msgid "_Lock Volume"
msgstr "_Kunci Volum"

#: ../src/xfdesktop-volume-icon.c:906
msgid "E_ject Volume"
msgstr "K_eluarkan Volum"

#: ../src/xfdesktop-volume-icon.c:915
msgid "_Unmount Volume"
msgstr "Nyahlekap Vol_um"

#: ../src/xfdesktop-volume-icon.c:922
msgid "_Mount Volume"
msgstr "Lekap Volu_m"

#: ../src/xfdesktop-window-icon.c:193
msgid "_Window Actions"
msgstr "Tindakan Tetingkap"
